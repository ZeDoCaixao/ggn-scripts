// ==UserScript==
// @name         GGn hide group info on upload
// @version      0.3
// @description  makes uploading more minimalistic
// @author       ZeDoCaixao
// @match        https://gazellegames.net/upload.php?groupid=*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.getElementById("groupinfo").style.display = 'none';
    document.getElementById("torrent_type").style.display = 'none';
    document.querySelector("#releaseinfo .colhead").style.display = 'none';
    let gameTitle = $('#title').val();
    let platform = $('#platform').val();
    let groupId = $('input[name="groupid"]').val();
    let platformLink = '<a href="https://gazellegames.net/artist.php?artistname=' + platform+ '">' + platform + '</a>';
    let gameLink = '<a href="https://gazellegames.net/torrents.php?id='+groupId+'">'+gameTitle+'</a>'
    $('#torrent_properties').children('tbody').children('.colhead').children('td')
      .html('Uploading to: '+platformLink+' - '+gameLink);
})();
