// ==UserScript==
// @name         GGn compacter upload
// @version      0.2
// @description  Makes uploading more minimalistic
// @author       ZeDoCaixao
// @match        https://gazellegames.net/upload.php
// @grant        GM.xmlHttpRequest
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';
    let br = document.getElementById("dnu_header").parentNode.nextElementSibling;
    br.style.display = 'none';
    br.nextElementSibling.style.display = 'none';
    br.nextElementSibling.nextElementSibling.style.display = 'none';
    br.nextElementSibling.nextElementSibling.nextElementSibling.style.display = 'none';
    document.getElementById("announce_uri").style.display = 'none';
    let inp = document.querySelector("#rlstitle td input");
    let td = inp.parentNode;
    td.innerHTML = '';
    td.appendChild(inp);
    document.querySelector("#title_tr td p.min_padding").style.display = 'none';
    document.getElementById("scene_autoselect_info").style.display = 'none';
    document.getElementById("release_desc").parentNode.querySelector("p.min_padding").style.display = 'none';
    document.querySelectorAll("a[href='rules.php?p=upload']")[0].parentNode.style.display = 'none';
    document.querySelectorAll("a[href='rules.php?p=upload']")[0].parentNode.nextElementSibling.style.display = 'none';
})();
