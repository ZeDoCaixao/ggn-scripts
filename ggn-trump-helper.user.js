// ==UserScript==
// @name         GGn Trump Helper
// @namespace    http://tampermonkey.net/
// @version      0.3.2
// @match        https://gazellegames.net/torrents.php?id=*
// @require      https://code.jquery.com/jquery-3.1.0.min.js
// @grant        GM.xmlHttpRequest
// @grant        GM_xmlhttpRequest
// ==/UserScript==
/* globals jQuery, $ */

const th_predefs = [
  ["VERSION", "New version"],
  ["GOODIES", "Updated goodies"],
  ["VERSION-EA", "New version (3 latest builds)"],
  ["VERIFIED", "Trumped by verified dump"],
  ["CAPS", "Trumped by upload with track and titles following Capitalization Guidelines"],
];


function add_reports_helper_button(e, label, text) {
    $(e).after( '<a href="javascript:;" id="th_'+label+'">'+label+'</a><br/> ');
    $("#th_"+label).click(function() {
      $("#rp_helper #extra").html(text);
    });
}

function add_report_helper() {
    $('a[title="Report"]').each(function() {
        var torrent_id = /&id=([0-9]+)/.exec($(this).attr("href"))[1];
        console.log(torrent_id);
        var token = new Date().getTime();
        var form = `<tr id="rp_helper"><td>
          <form action="/reportsv2.php?action=takereport" enctype="multipart/form-data" method="post" id="report_table">
          <input type="hidden" name="submit" value="true">
          <input type="hidden" name="torrentid" value="`+torrent_id+`">
          <input type="hidden" name="categoryid" value="1">
          <input type="hidden" name="type" value="trump">
          <input type="hidden" name="type" value="trump">
          <input id="sitelink" type="hidden" name="sitelink" size="70" value="">
          <textarea id="extra" rows="5" cols="60" name="extra"/>
          <input type="submit" value="Submit report">
          <input type="hidden" name="id_token" value="`+token+`"/>
          </form></td>
          <td><span id="trumphelper_predefs"></div></td></tr>`;
        th_predefs.forEach(function(e, i, v) {
          $("#trumphelper_predefs").after(e[0] + e[1])
        });
        $(this).after(
            ' | <a href="javascript:;" title="Trump" id="rp_'
            +torrent_id
            +'">TP');
        $('#rp_'+torrent_id).click(function (event) {
            $("#rp_helper").remove();
            $(this).closest("tr").after(form);
            convert_pls();
            th_predefs.forEach(function(e,i,v) {
              add_reports_helper_button($("#trumphelper_predefs"), e[0], e[1]);
            });
        });
    });
}

function convert_pls() {
    $('a[title="Permalink"]').each(function() {
        var url = $(this).attr("href");
        $(this).click(function (event) {
            event.preventDefault();
            $(this).toggleClass("rp_good");
            $('a[title="Permalink"]').css({color: ''});
            $('.rp_good').css({color: "red"});
            var urls = "";
            $('.rp_good').each(function() {
                urls += " https://gazellegames.net/" + $(this).attr("href");
            });
            $("#rp_helper #sitelink").val(urls);
        });
    });
}

(function() {
    'use strict';
    add_report_helper();
})();
